#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include "Sac/sac.hpp"

using namespace std;

Sac lireFichier(char *monFichierString,vector<Objet> &objets);

int main(int argc, char *argv[])
{

  if(argc != 2)
  {
    cout << "Manque fichier comme argument" << endl;
    return 0;
  }
  cout.precision(10);

  vector<Objet> objets;
  Sac sac = lireFichier(argv[1], objets);


  Sac::afficheObjets(objets);

  //sac.addListeObjetGlouton(objets);

  //sac.addListeObjetGloutonCoupe(objets);

  sac.addListeObjetBestListe(objets);

  return 0;
}

Sac lireFichier(char *monFichierString,vector<Objet> &objets)
{
  ifstream monFichier;
  monFichier.open(monFichierString);

  string ligne;
  getline(monFichier,ligne);
  string capacite = ligne;

  while (getline(monFichier,ligne)) {
    string nombrePoid = ligne.substr(0, ligne.find(" "));
    string nombreValeur = ligne.substr(ligne.find(" ")+1, ligne.length());
    objets.push_back(Objet(std::stod(nombreValeur),std::stod(nombrePoid)));
  }

  monFichier.close();
  return Sac(std::atoi(capacite.c_str()));
}
