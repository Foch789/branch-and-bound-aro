#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "../Objet/objet.hpp"

using namespace std;

class Sac
{

public :
  int nbrObjet;
  int capaciteMax;
  int capacite;
  double valeurSac;
  vector<Objet> objets;

  Sac(int _capaciteMax);
  ~Sac();

  void addObjetCoupe(Objet &objet);
  void addObjet(Objet &objet);

  void addListeObjetGlouton(vector<Objet> objets_param);
  void addListeObjetGloutonCoupe(vector<Objet> objets_param);
  void addListeObjetBestListe(vector<Objet> objets_param);

  void supprObjet(int i);
  void supprTout();

  void afficheInfo();
  void afficheSac();
  void static afficheObjets(vector<Objet> objets_param);

private :
  void prendObjet(Sac &sac_temp, vector<Objet> &objs, int posSuivante);
  void laisseObjet(Sac &sac_temp, vector<Objet> &objs, int posSuivante);

  int valeurBranche(vector<Objet> &objs, int aPartirDe);
  int valeurPoid(vector<Objet> &objs);
  double valeurCoupe(int capaciteSac, vector<Objet> &objs, int posDepart);

};
