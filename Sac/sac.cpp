#ifndef SAC_HPP
#define SAC_HPP
#include "sac.hpp"

using namespace std;

//Fonction autre
bool compareObjets(Objet A, Objet B)
{
  return A.ratio >  B.ratio;
}
//Fonction class Sac

Sac::Sac(int _capacity):nbrObjet(0),capaciteMax(_capacity),capacite(0.0),valeurSac(0.0)
{}

Sac::~Sac()
{
  supprTout();
}

void Sac::addObjetCoupe(Objet &objet)
{
  if (capaciteMax >= capacite+objet.poid)
  {
    nbrObjet++;
    capacite += objet.poid;
    valeurSac += objet.valeur;
    objets.push_back(objet);
  }
  else if ((capaciteMax >= capacite) & (capaciteMax <= capacite+objet.poid))
  {
    int poidR = objet.poid - ((capacite+objet.poid) - capaciteMax);
    int valeurR = int(objet.ratio * poidR);
    int idR = int(poidR)/int(objet.poid);

    nbrObjet += idR;
    capacite += poidR;
    valeurSac += valeurR;
    objets.push_back(Objet(valeurR,poidR));
  }
}

void Sac::addObjet(Objet &objet)
{

  if (capaciteMax >= capacite+objet.poid)
  {
    nbrObjet++;
    capacite += objet.poid;
    valeurSac += objet.valeur;
    objets.push_back(objet);
  }
}

void Sac::addListeObjetGlouton(std::vector<Objet> objets_param)
{
  supprTout();
  std::sort(objets_param.begin(),objets_param.end(),compareObjets);

  for(unsigned int i = 0; i < objets_param.size(); i++)
  {
    addObjet(objets_param[i]);
  }

  afficheSac();
  afficheInfo();
}

void Sac::addListeObjetGloutonCoupe(std::vector<Objet> objets_param)
{
  supprTout();
  std::sort(objets_param.begin(),objets_param.end(),compareObjets);

  for(unsigned int i = 0; i < objets_param.size(); i++)
  {
    addObjetCoupe(objets_param[i]);
  }

  afficheSac();
  afficheInfo();
}


void Sac::addListeObjetBestListe(std::vector<Objet> objets_param)
{
  supprTout();
  std::sort(objets_param.begin(),objets_param.end(),compareObjets);

  Sac sac_temp(capaciteMax);

  prendObjet(sac_temp, objets_param, 0);
  laisseObjet(sac_temp, objets_param, 0);

  afficheSac();
  afficheInfo();

}

void Sac::prendObjet(Sac &sac_temp, vector<Objet> &objs, int posSuivante)
{
  sac_temp.addObjet(objs[posSuivante]);

  if (valeurSac < sac_temp.valeurSac)
  {
    nbrObjet = sac_temp.nbrObjet;
    valeurSac = sac_temp.valeurSac;
    capacite = sac_temp.capacite;
    objets = sac_temp.objets;
  }

  if (size_t(posSuivante+1) < objs.size())
  {
    if( valeurSac <= (sac_temp.valeurSac+valeurCoupe(sac_temp.capacite, objs, posSuivante+1)))
    {
      if(capaciteMax >= (sac_temp.capacite + objs[posSuivante+1].poid))
      {
        prendObjet(sac_temp, objs, posSuivante+1);
        laisseObjet(sac_temp, objs, posSuivante+1);
      }
      else
      {
        laisseObjet(sac_temp, objs, posSuivante+1);
      }
    }
  }

  sac_temp.supprObjet(sac_temp.objets.size()-1);

}

void Sac::laisseObjet(Sac &sac_temp, vector<Objet> &objs, int posSuivante)
{
  if (size_t(posSuivante+1) < objs.size())
  {
    if( valeurSac <= (sac_temp.valeurSac+valeurCoupe(sac_temp.capacite, objs, posSuivante+1)))
    {
      if(capaciteMax >= (sac_temp.capacite + objs[posSuivante+1].poid))
      {
        prendObjet(sac_temp, objs, posSuivante+1);
        laisseObjet(sac_temp, objs, posSuivante+1);
      }
      else
      {
        laisseObjet(sac_temp, objs, posSuivante+1);
      }
    }
  }
}

double Sac::valeurCoupe(int capaciteSac, vector<Objet> &objs, int posDepart)
{
  double valeurRetour = 0.0;
  int capaciteTemp = capaciteSac;

  for (size_t i = posDepart; i < objs.size(); i++) {
    if (capaciteMax >= capaciteTemp+objs[i].poid)
    {
      capaciteTemp += objs[i].poid;
      valeurRetour += objs[i].valeur;
    }
    else if( (capaciteMax >= capaciteTemp) & (capaciteMax <= capaciteTemp+objs[i].poid))
    {
      int poidR = objs[i].poid - ((capaciteTemp+objs[i].poid) - capaciteMax);
      double valeurR = double(objs[i].ratio * poidR);

      capaciteTemp += poidR;
      valeurRetour += valeurR;
    }
  }

  return valeurRetour;
}

int Sac::valeurBranche(vector<Objet> &objs, int aPartirDe)
{
  int valeur = 0;
  for (size_t i = aPartirDe; i < objs.size(); i++) {
    valeur += objs[i].valeur;
  }
  return valeur;
}

int Sac::valeurPoid(vector<Objet> &objs)
{
  int valeur = 0;
  for (size_t i = 0; i < objs.size(); i++) {
    valeur += objs[i].poid;
  }
  return valeur;
}

void Sac::supprObjet(int i)
{
  capacite = capacite - objets[i].poid;
  valeurSac = valeurSac - objets[i].valeur;
  objets.erase(objets.begin()+i);
  nbrObjet--;
}

void Sac::supprTout()
{
  objets.clear();
  nbrObjet = 0;
  capacite = 0;
  valeurSac = 0;
}

void Sac::afficheInfo()
{
  cout << "==========================afficheInfo==========================" << endl << endl;
  cout << "Sac : " << endl;
  cout << "{" << endl;
  cout << "nbrObjet = " << nbrObjet << endl;
  cout << "Capacite max = " << capaciteMax << endl;
  cout << "Capacite actuelle = " << capacite << endl;
  cout << "Valeur du sac = " << valeurSac << endl;
  cout << "}" << endl;
  cout << "===============================================================" << endl << endl;
}

void Sac::afficheSac()
{

  if(nbrObjet > 0)
  {
    cout << "==========================afficheSac===========================" << endl << endl;
    for (size_t i = 0; i < size_t(nbrObjet); i++) {
      objets[i].simpleAfficheInfo();
    }
    cout << "==============================================================" << endl << endl;
  }
  else
  {
    cout << "Pas d'objets dans le sac." << endl;
  }
}

void Sac::afficheObjets(vector<Objet> objets_param)
{
  if(objets_param.size() > 0)
  {
    cout << "====================================================" << endl << endl;
    for (size_t i = 0; i < size_t(objets_param.size()); i++) {
      objets_param[i].simpleAfficheInfo();
    }
    cout << "====================================================" << endl << endl;
  }
  else
  {
    cout << "Pas d'objets." << endl;
  }
}

#endif
