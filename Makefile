Branch_And_Bound.exe: main.o sac.o objet.o
	g++ -o Branch_And_Bound main.o sac.o objet.o

main.o: main.cpp Sac/sac.cpp Objet/objet.cpp
	g++ -Wall -c -std=c++11 main.cpp Sac/sac.cpp Objet/objet.cpp

sac.o: Sac/sac.cpp Objet/objet.cpp Sac/sac.hpp
	g++ -Wall -c -std=c++11 Sac/sac.cpp Objet/objet.cpp

objet.o: Objet/objet.cpp Objet/objet.hpp
	g++ -Wall -c -std=c++11 Objet/objet.cpp

clean: cleanObjet
	rm Branch_And_Bound.exe | rm Branch_And_Bound

cleanObjet:
	rm *.o
