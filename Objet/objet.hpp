class Objet
{

  private :
  static unsigned int nbrObjet;


  public :
  int id;
  double ratio;
  double valeur;
  int poid;

  Objet(double _valeur, int _poid);
  ~Objet();

  void afficheInfo();
  void simpleAfficheInfo();

};
