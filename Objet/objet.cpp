#ifndef OBJET_HPP
#define OBJET_HPP
#include <iostream>
#include "objet.hpp"

using namespace std;

unsigned int Objet::nbrObjet = 0;

Objet::Objet(double _valeur, int _poid)
{

  id = nbrObjet;
  valeur = _valeur;
  poid = _poid;
  ratio = double(valeur)/double(poid);
  nbrObjet = nbrObjet + 1;
}

Objet::~Objet()
{
  // nbrObjet = nbrObjet - 1;
}

void Objet::afficheInfo()
{
  cout << "Objet id : " << id << endl;
  cout << "{" << endl;
  cout << "Valeur = " << valeur << endl;
  cout << "Poid = " << poid << endl;
  cout << "Ratio = " << ratio << endl;
  cout << "}" << endl;
}

void Objet::simpleAfficheInfo()
{
  cout << "Objet id : " << id << " / Valeur : " << valeur << " / Poid : "  << poid << endl;
}

#endif
